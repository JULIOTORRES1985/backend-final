'use strict'

var requestJson = require('request-json');
const config=require('../config');
const url=config.mlab_host+config.mlab_db+'collections/';
var generate_id=0;

//GET ALL USERS
function getUsers(request,response){
    console.log("Get de Usuarios ....");
    var client = requestJson.createClient(url);
    const queryName='f={"_id":0}&';
    client.get(config.mlab_collection_user+'?'+queryName+config.mlab_key, function(err, res, body) {
    generate_id=body.length;
    response.send(body);
  });
}

//GET USER BY EMAIL
function getUser(request,response){
    console.log("Get por email");
    var client = requestJson.createClient(url);
    const queryName='q={"email":"'+request.params.id+'"}&';
    client.get(config.mlab_collection_user+'?'+queryName+config.mlab_key, function(err, res, body) {
    var respuesta=body[0];
    if(undefined==respuesta)
      response.status(404).send({message:"Usuario no Existe"});
    else{
      console.log(respuesta);
      response.send(respuesta);
    }
  });
}

//POST USER
function saveUser(request,response){
      console.log("Creacion de Usuario");
      //VALIDATE INPUT
      request.checkBody("nombre", "Nombre esta vacio").notEmpty();
      request.checkBody("apellido", "Apellido esta vacio").notEmpty();
      request.checkBody("email", "Ingresar un email valido.").isEmail();
      request.checkBody('password', 'Password debe tener al menos 5 caracteres y un número').isLength({ min: 5 }).matches(/\d/);

      var errors = request.validationErrors();
      if (errors) {response.status(400).send(errors);return;}

      var data = {
      "idCliente" : generate_id+1,
      "nombre" : request.body.nombre,
      "apellido" : request.body.apellido,
      "email" : request.body.email,
      "password" : request.body.password,
      "perfil" : request.body.perfil
    };
    var client = requestJson.createClient(url);
    client.post(config.mlab_collection_user+'?'+config.mlab_key, data, function(err, res, body) {
    if(err)console.log(err);
    response.send(body);
    });
};

//PUT USER
function updateUser(request,response){
    console.log("Actualizacion de Usuario");
    request.checkBody("nombre", "nombre no puede ser vacio").notEmpty();
    request.checkBody("apellido", "apellido no puede ser vacio").notEmpty();
    request.checkBody("email", "Ingrasar email valido.").isEmail();

    var errors = request.validationErrors();
    if (errors) {response.status(400).send(errors);return;}

    var clienteMlab = requestJson.createClient(url);
    var cambio = '{"$set":' + JSON.stringify(request.body) + '}';
    clienteMlab.put(config.mlab_collection_user+'?q={"idCliente": ' + request.params.id + '}&' + config.mlab_key, JSON.parse(cambio), function(err, resM, body) {
    response.send(body);
    });
};

//DELETE USER
function removeUser(request,response){
    console.log("Borrado de Usuario");
     var client = requestJson.createClient(url);
     const queryName='q={"idCliente":'+request.params.id+'}&';
     client.get(config.mlab_collection_user+'?'+queryName+config.mlab_key, function(err, res, body) {
       var respuesta=body[0];
       client.delete(config.mlab_collection_user+'/'+respuesta._id.$oid+'?'+config.mlab_key, function(errD, resD, bodyD) {
         if(errD)console.log(errD);
          response.send(bodyD);
       });
     });
};


module.exports={
  getUsers,
  getUser,
  saveUser,
  updateUser,
  removeUser
};
